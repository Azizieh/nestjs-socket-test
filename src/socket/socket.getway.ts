import { ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { Socket } from 'dgram';


@WebSocketGateway()
export class SocketGateway implements OnGatewayConnection {


    @WebSocketServer()
    server: Server;


    handleConnection(client: Socket, request) {
        setTimeout(() => {
            client.emit("test", {
                massage: "test massage"
            })
        }, 3000)
    }
}
