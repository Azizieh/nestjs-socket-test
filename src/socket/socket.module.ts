import { Module } from '@nestjs/common';
import { SocketGateway } from './socket.getway';

@Module({
    providers: [SocketGateway]
})
export class SocketModule { }
